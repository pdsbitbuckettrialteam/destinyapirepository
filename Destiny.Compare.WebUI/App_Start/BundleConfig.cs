﻿
namespace Destiny.Compare.WebUI
{
    #region usings

    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web;
    using System.Web.Optimization;

    #endregion

    public class BundleConfig
    {
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new StyleBundle("~/css/base").Include("~/content/css/base.css"));
        }
    }
}