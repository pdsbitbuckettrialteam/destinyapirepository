﻿
namespace Destiny.Compare.WebUI.Controllers
{
    #region usings

    using Destiny.Compare.WebUI.Models.Home;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web;
    using System.Web.Mvc;

    #endregion

    /// <summary>
    /// 
    /// </summary>
    [RoutePrefix("")]
    public class HomeController : Controller
    {
        #region action methods

        [Route("")]
        public ActionResult Index()
        {
            IndexVM vm = new IndexVM("Home");
            return this.View(vm);
        }

        #endregion
    }
}