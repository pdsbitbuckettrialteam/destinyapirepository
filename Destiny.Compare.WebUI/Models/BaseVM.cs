﻿
namespace Destiny.Compare.WebUI.Models
{
    #region usings

    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web;

    #endregion

    public abstract class BaseVM
    {
        #region properties

        public string PageTitle { get; set; }

        #endregion

        #region constructors

        public BaseVM(string pageTitle)
        {
            if (string.IsNullOrWhiteSpace(pageTitle))
            {
                throw new ArgumentNullException(nameof(pageTitle));
            }

            this.PageTitle = pageTitle;
        }

        #endregion
    }
}